import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LawnMower {
   

    public static void processInputFile(String inputFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            String[] dimensions = br.readLine().split(" ");
            int maxX = Integer.parseInt(dimensions[0]);
            int maxY = Integer.parseInt(dimensions[1]);

            String line;
            while ((line = br.readLine()) != null) {
                String[] initialPosition = line.split(" ");
                int x = Integer.parseInt(initialPosition[0]);
                int y = Integer.parseInt(initialPosition[1]);
                char orientation = initialPosition[2].charAt(0);

                String instructions = br.readLine();

                processInstructions(x, y, maxX, maxY, orientation, instructions);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void processInstructions(int x, int y, int maxX, int maxY, char orientation, String instructions) {
        for (char instruction : instructions.toCharArray()) {
            switch (instruction) {
                case 'G':
                    orientation = rotateLeft(orientation);
                    break;
                case 'D':
                    orientation = rotateRight(orientation);
                    break;
                case 'A':
                    int newX = x;
                    int newY = y;
                    switch (orientation) {
                        case 'N':
                            newY = Math.min(newY + 1, maxY);
                            break;
                        case 'E':
                            newX = Math.min(newX + 1, maxX);
                            break;
                        case 'S':
                            newY = Math.max(newY - 1, 0);
                            break;
                        case 'W':
                            newX = Math.max(newX - 1, 0);
                            break;
                    }
                    x = newX;
                    y = newY;
                    break;
            }
        }

        // Affiche la position finale de la tondeuse
        System.out.println(x + " " + y + " " + orientation);
    }

    public static char rotateLeft(char orientation) {
        switch (orientation) {
            case 'N':
                return 'W';
            case 'E':
                return 'N';
            case 'S':
                return 'E';
            case 'W':
                return 'S';
            default:
                return orientation;
        }
    }

    public static char rotateRight(char orientation) {
        switch (orientation) {
            case 'N':
                return 'E';
            case 'E':
                return 'S';
            case 'S':
                return 'W';
            case 'W':
                return 'N';
            default:
                return orientation;
        }
    }
    
    public static void main(String[] args) {
        processInputFile("/Users/compte/Desktop/instructions.txt");
    }
}
